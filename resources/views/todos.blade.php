@extends('layout')
    @section('content')
        <div class="row">
            <div class="col-lg-6 col-offset-3">
                <form action="/create/todo" method="post">
                {{ csrf_field() }}
                    <input type="text" name="todo" class="form-control input-lg" placeholder="Create a new todo">
                </form>
            </div>
        </div>
        <hr>
        @foreach($todos as $todo)
            {{$todo->todo}} <a href="{{route('todo.delete',['id' => $todo->id])}}" class="btn btn-danger">X</a>
            <a href="{{route('todo.update',['id' => $todo->id])}}" class="btn btn-info btn-xs">update</a>
            @if(!$todo->completed)
            <a href="{{route('todo.completed',['id' => $todo->id])}}" class="btn btn-success btn-xs">Mark as completed</a>
            @else
                <span class="small-text">completed</span>
            @endif
        
            <hr>
        @endforeach
    @endsection